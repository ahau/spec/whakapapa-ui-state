# whakapapa-ui-state

:warning: the following is a DRAFT PROPOSAL and not reflective of the current state

## Requirements

- smooth render + render updates
    - incremental loading
    - updates on:
        - profile crut
        - link crut
        - view crut
        - other people editing?
- easy to maintain + extend
    - testable
    - easy to reason about

## Propsal

Currently we have different sorts of data all mixed together.
I'd like use to seperate out how the data is stored and computed

```js
// vuex state

const state = {
  view: {
    id,
    focus,
    importantRelationship
  },
  profiles: {
    [profileId]: Profile
  },
  links: {
    [profileId] {
      parents: [
        {
          id,                // parent profileId
          linkId,            // used for editing
          linkType: LinkType // birth|whangai|adopted
          //                    space for more data, like author, verified etc.
        }
      ],
      children: [
        { id, linkId, linkType }
      ],
      partners: [
        { id, linkId, linkType }
      ],

      inferred: {
        partners: [],         // not explicitly linked, but you have a child together?
        children: []          // other partners children??
      }
    }
  },
  coordinates: {
    [profileId + parentId]: { // TODO we need a unique id for referencing a profile in a particular place in the graph...
      x,
      y,
      partners: [
        { id, x, y }
      ]
    }
  }
}
```

So we take the `links` and build a *minimal* `tree`, starting from the `view.focus`,
which we then feed to d3 to generate coordinates, which we store in state.

Note we don't need profile data, only the skeleton on links.

When we come to drawing the graph we use:
- `tree` - to know how to iterate through
- `links` - to know what should be drawn next to each node (partners etc)
- `coorindates` - to know where things should be drawn
- `profiles` - to know what each node should be drawn like


## TODO

1. review the current state management and fold in all the bits of state being decorated in,
    - e.g. ghost nodes, isPartner, isNonPartner, isDuplicate, isCollapsed, ...

2. review links below
    - should perhaps split all that state into different vuex modules!
        - e.g. profile, relationships (links), whakapapa (view, coordinates)
        - maybe coordinates doesn't need to be in vuex... ? TBD

3. investigate how to guarentee redraws while mimising re-calculation
    - e.g. data structures which make watching cheaper/ smarter
    - e.g. memoising?
    

## Research / References

1. Flatten vuex state:
    - https://markus.oberlehner.net/blog/make-your-vuex-state-flat-state-normalization-with-vuex/
    - use https://vuejs.org/v2/api/#Vue-set

2. Vuex context/ provider pattern
    - https://markus.oberlehner.net/blog/context-and-provider-pattern-with-the-vue-3-composition-api/
    - https://v3.vuejs.org/guide/component-provide-inject.html

3. SWR pattern (stale which re-validate)
    - using apollo-cache
    - https://markus.oberlehner.net/blog/application-state-management-with-vue-3/#use-the-swr-cache-pattern
    - https://github.com/Kong/swrv


